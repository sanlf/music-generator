#!/bin/bash

#test=$( ls weights* | tail -n 1 )

#ln -s "$test" "weights.hdf5"
zelda=(
    "majoras"
    "waker"
    "twilight"
)

source activate keras

for game in ${zelda[@]};
do
    echo "Starting $game"

    mkdir -p "results/$game/midi"
    mkdir -p "results/$game/log"
    mkdir -p "results/$game/weights"

    #move the game songs
    rm -r midi_songs
    cp -r "zelda_midi/*$game*" "midi_songs"

    python lstm.py > "results/$game/log/retraining1.log"

    best_weight=$(ls weights* | tail -n 1)
    ln -s "$best_weight" "weights.hdf5"

    python predict.py
    mv test_output.mid "results/$game/midi/test_output1.mid"

    python predict.py
    mv test_output.mid "results/$game/midi/test_output2.mid"

    python predict.py
    mv test_output.mid "results/$game/midi/test_output3.mid"

    rm weights.hdf5

    mv weights* "results/$game/weights"

    echo "Finishing $game"
done

